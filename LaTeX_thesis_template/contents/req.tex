Das Hauptziel dieser Arbeit besteht darin, die Benutzbarkeit der Webanwendung QDAcity bei Verbindungsverlust
zu gewährleisten. Das bedeutet, eine definierte Menge von Funktionalitäten muss auch
ohne aktive Internetverbindung erfolgreich vom Benutzer der Anwendung ausgeführt werden können.
Ist er wieder online, soll er, nachdem seine Änderungen synchronisiert wurden, wie erwartet weiterarbeiten können.
Um diese grobe Anforderung genauer betrachten zu können, wurden folgende, detailliertere Anforderungen erstellt.
Mit diesen Anforderungen kann sichergestellt werden, dass die Software ihre Funktion erfüllt
und angemessen entwickelt wurde.
Tabelle \ref{table:def_req} erklärt die in den Anforderungen benutzten Terme.

\begin{table}[htb]
    \centering
    \begin{tabular}{p{0.25\textwidth}@{\hskip0.15\textwidth}p{0.6\textwidth}}
        \textbf{Term} & \textbf{Semantische Bedeutung} \\
        \hline \hline
        Benutzer & Ein \textsl{Benutzer} ist eine physische Person, die mit der Anwendung interagiert\\
        \hline
        Aktion & Eine \textsl{Aktion} ist eine Interaktion des \textsl{Benutzers} mit der Anwendung. Diese kann nur lesend sein,
        z.B.\ das Laden der Editor-Seite, oder schreibend, z.B das Hinzufügen eines Codes initiiert durch einen Button-Klick \\
        \hline
        lokale Aktion & Eine \textsl{lokale Aktion} ist eine \textsl{Aktion}, die ohne aktive Internetverbindung
        ausgeführt wurde\\
        \hline
        Operation & Eine \textsl{Operation} ist ein HTTP Request oder eine Socket Message, die für eine \textsl{Aktion}
        notwendig sein können\\
        \hline
        SW & Der \textsl{SW} sei der Service Worker, der im Rahmen dieser Thesis implementiert wird und den Großteil zur
        Bereitstellung der Offline Benutzbarkeit der Anwendung beiträgt\\
        \hline
        Backend & Backend ist die Backend Komponente von QDAcity \\
        \hline
        Ressource & Eine \textsl{Ressource} ist ein Entität, die im \textsl{Backend} persistiert ist und deren Zustand
        sich ändern kann, beispielsweise ein Code\\
        \hline
        unterstützte Aktion & Eine \textsl{unterstützte Aktion} beschreibt eine \textsl{Aktion}, die auch ohne
        aktive Internetverbindung das erwartete Verhalten aufweist\\
        \hline
        nicht unterstützte Aktion & Eine \textsl{nicht unterstützte Aktion} beschreibt eine \textsl{Aktion}, die ohne
        aktive Internetverbindung nicht funktioniert\\
        \hline
        Synchronisation & Eine \textsl{Synchronisation} ist der Prozess, bei dem die durch \textsl{lokale Aktionen}
        des \textsl{Benutzers}
        entstandene \textsl{Operationen} und Zustandsänderungen von \textsl{Ressourcen} so verarbeitet werden müssen,
        dass diese auf dem Backend widergespiegelt werden können \\
        \hline
        Konflikt & Ein \textsl{Konflikt} entsteht genau dann, wenn während der \textsl{Synchronisation} festgestellt
        wird, dass der Zustand der \textsl{Ressource} im Backend, auf die die \textsl{Operation} angewendet werden soll,
        sich vom Zustand der \textsl{Ressource} vor der lokalen Ausführung der \textsl{Operation} unterscheidet\\
    \end{tabular}
    \caption{Definition der Terme, die in den Anforderungen benutzt werden}
    \label{table:def_req}
\end{table}

\reqinit

\section{Funktionale Anforderungen}\label{sec:req_func}
Die hier gelisteten funktionale Anforderungen folgen der Schablone FunktionsMAST{\small E}R
der SOPHISTen \autocite{sophist}.
Abbildung \ref{fig:func_master} beschreibt den Aufbau einer einzelnen Anforderung.
\begin{figure}[htbp]
    \centering
    \includegraphics[width=\linewidth]{function_master.pdf}
    \caption{Schablone FunktionsMAST{\small E}R}
    \label{fig:func_master}
\end{figure}
Die Knoten, deren Bezeichnung in spitzen Klammern dargestellt ist, sind Platzhalter und werden
mit tatsächlichen Ausdrücken ersetzt.
Die Schlüsselwörter \textsl{muss}, \textsl{sollte} und \textsl{wird}
werden in Tabelle \ref{table:def_keywords} definiert.

\begin{table}[H]
    \centering
    \begin{tabular}{p{0.25\textwidth}@{\hskip0.15\textwidth}p{0.6\textwidth}}
        \textbf{Term} & \textbf{Semantische Bedeutung} \\
        \hline \hline
        Muss & Die Anforderung muss erfüllt werden, damit die Software ihre Funktion erfüllt \\
        \hline
        Sollte & Die Anforderung ist wichtig, die Erfüllung stellt aber keine notwendige Bedingung für
        die Funktionalität der Software dar\\
        \hline
        Wird & Die Anforderung ist nice-to-have \\
    \end{tabular}
    \caption{Definition der Schlüsselworte}
    \label{table:def_keywords}
\end{table}



\subsection{Allgemein}\label{subsec:req_general}
\reqstart

\item Sobald der Benutzer offline ist, muss die Anwendung dem Benutzer dies kenntlich machen.
Wenn er wieder online ist, muss das ebenfalls in der Anwendung dem Benutzer ersichtlich sein.
\label{req:show_offline}

\reqend

\subsection{Lokale Aktionen}\label{subsec:req_local}
\reqstart

\item Sobald der Benutzer eine lokale, nicht unterstützte Aktion ausführt,
muss die Anwendung dem Benutzer kenntlich machen, dass die Aktion offline nicht unterstützt wird.
\label{req:loccal_start}

\item Sobald der Benutzer eine lokale, unterstützte Aktion ausführt,
muss die Anwendung mithilfe des Service Workers die entstandenen Operationen ausführen.
Die Ausführung der Operationen und die daraus resultierende Antwort muss identisch mit
der Antwort des Backends sein.

\item Sobald der Benutzer eine lokale, unterstützte Aktion ausführt,
muss dem Benutzer kenntlich gemacht werden, dass diese Aktion nur lokal ausgeführt wurde.

\item Sobald der Benutzer mindestens eine lokale, unterstützte Aktion ausgeführt hat,
muss dem Benutzer eine Liste aller lokalen Aktionen angezeigt werden.
\label{req:loccal_end}

\reqend

\subsection{Synchronisation}\label{subsec:req_sync}
\reqstart

\item Sobald der Benutzer eine lokale, unterstützte Aktion ausführt,
muss sich die Anwendung diese für die Synchronisation merken.

\item Sobald der Benutzer, nachdem er offline war, wieder online ist, muss er erneut authentifiziert werden.

\item Sobald der Benutzer, nachdem er offline war, wieder online ist, muss die Synchronisation gestartet werden.

\item Aktionen, die durch den RTCS unterstützt werden,
müssen auch während der Synchronisation mit diesem ausgeführt werden.

\item Sobald eine Operation für die Synchronisierung gespeichert wird, wird die Liste der Operationen vereinfacht.
Vereinfacht bedeutet, dass Operationen, deren Änderung durch folgende Operationen über-schrieben werden,
entfernt werden.

\item Nachdem die Synchronisation erfolgreich beendet wurde, müssen die aktuellen Ressourcen in die Frontend Anwendungen
geladen werden.


\reqend

\subsection{Konfliktbehandlung}\label{subsec:req_conflict}
\reqstart

\item Sobald bei der Synchronisation ein Konflikt entsteht, muss die Anwendung diesen erkennen.

\item Sobald bei der Synchronisation ein Konflikt entsteht, muss dem Benutzer die Möglichkeit gegeben werden,
den Konflikt beheben zu können, indem entweder a) die aktuelle Synchronisationsoperation mit der eigenen Änderung
ausgeführt wird, oder b) der neuen Stand im Backend akzeptiert wird

\item Sobald ein Konflikt erkannt wird, die Änderungen aber einfach zusammengeführt werden können, sollte dies als dritte
Option der Konfliktbehandlung verfügbar sein.

\reqend

\subsection{Unterstützte Aktionen}\label{subsec:req_conflict}
Die folgende Anforderung folgt nicht dem bisherigen Schema FunktionsMaster.
Die in den Aktionen verwendeten Begriffe sind im Anhang in der Tabelle \ref{table:action_terms} definiert.
\reqstart
\item Die in Liste \ref{list:supported_reqs} gelisteten Aktionen sind Elemente der Menge der \textsl{unterstützten Aktionen}.
\reqend
\begin{figure}[H]
\begin{enumerate}
    \item Hinzufügen eines Codings
    \item Entfernen eines Codings
    \item Bearbeiten eines Codes
    \item Hinzufügen eines Codes
    \item Entfernen eines Codes
    \item Verschieben eines Codes
    \item Hinzufügen eines Dokumentes
    \item Entfernen eines Dokumentes
\end{enumerate}
\caption{Liste der unterstützten Aktionen, absteigend priorisiert}
\label{list:supported_reqs}
\end{figure}



\section{Nicht-Funktionale Anforderungen}\label{sec:req_non_func}
Die Liste der nicht-funktionalen Anforderungen orientiert sich an der Norm ISO/IEC 25010:2011 \autocite{ISO:25010}.
Tabelle \ref{table:iso25010} beschreibt eine Teilmenge der Anforderungen, die hier Verwendung finden.

\begin{table}[htpb]
    \centering
    \begin{tabular}{p{0.2\textwidth}@{\hskip0.05\textwidth}p{0.3\textwidth}@{\hskip0.0\textwidth}p{0.35\textwidth}}
        \textbf{Charakteristik} & \textbf{Sub-Charakteristik} & \textbf{Definition} \\
        \hline \hline
        Maintainability & Modifiability & Grad, in dem die Software effizient angepasst oder erweitert werden kann. \\
        & Reusability & Grad, in dem Komponenten der Software in mehreren Komponenten verwendet werden können. \\
        & Modularity & Grad, in dem die Software modularisiert ist, sodass die Veränderung
        einzelner Module minimale Auswirkung auf die anderen Module hat. \\
        \hline
        Security & Authenticity & Grad, in dem ein Objekt tatsächlich dem entspricht, was es angibt zu sein. \\
        & Confidentiality & Grad, in dem Daten nur Autorisierten Zugang gewährt wird. \\
        \hline
        Portability & Installability & Grad, in dem die Software effizient in einer Umgebung installiert werden kann.\\
        \hline
        Performance & Time Behaviour & Grad, in dem die Antwortzeiten der Software den Anforderungen entspricht.\\
        & Resource Utilization & Grad, in dem die Menge der benötigten Ressourcen der Software denen der Anforderung entspricht.\\
    \end{tabular}
    \caption{Nicht-Funktionale Anforderungen, die in den hier benutzten Anforderungen Verwendung finden. Definition nach ISO/IEC 25010:2011 }
    \label{table:iso25010}
\end{table}

Der Aufbau der Anforderungen folgt der Schablone EigenschaftsMAST{\small E}R und UmgebungsMAST{\small E}R
der SOPHISTen \autocite{sophist} und ist in Abbildung \ref{fig:property_master},
sowie Abbildung \ref{fig:property_master} dargestellt.

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{property_master.pdf}
    \caption{Schablone EigenschaftsMAST{\small E}R}
    \label{fig:property_master}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{env_master.pdf}
    \caption{Schablone UmgebungsMAST{\small E}R}
    \label{fig:property_master}
\end{figure}

\subsection{Technologische Anforderungen}\label{subsec:req_tech}
Der im Folgenden verwendete Begriff \textsl{Erweiterung} bezieht sich auf die Gesamtheit der Änderungen des Quellcodes,
die notwendig sind, um die in Abschnitt \ref{sec:req_func} definierten Anforderungen zu erfüllen.
\textsl{Software} meint die Anwendung der Änderungen der Erweiterung auf das ursprüngliche Produkt.
\reqstart
\item Die Erweiterung muss mittels eines Service Workers implementiert sein.
\item Der Service Worker muss so gestaltet sein, dass er einfach erweitert werden kann (Modifiability).
\item Der Service Worker muss so gestaltet sein, dass er entkoppelt vom bestehenden Produkt entwickelt werden kann (Modularity).
\item Die Software soll so gestaltet sein, dass gemeinsam nutzbarer Code von
den entsprechenden Komponenten genutzt und nicht mehrfach definiert wird (Reusability).
\item Der Service Worker muss so gestaltet sein, dass er in das bestehende Build-System integriert werden kann (Installability).
\item Die Erweiterung muss so gestaltet sein, das sie mit dem bestehenden Test-System getestet werden kann (Testability).
\item Die Erweiterung wird so gestaltet sein, dass die Daten der Nutzer nicht von unberechtigten Nutzern einsehbar sind (Confidentiality).
\item Die Erweiterung muss so gestaltet sein, dass der Erzeuger einer Aktion dem tatsächlichen Erzeuger/Nutzer entspricht (Authenticity).
\reqend

\subsection{Anwenderbezogene Anforderungen}\label{subsec:req_user}
\reqstart
\item Der dynamisch beanspruchte Festplattenspeicher der durch die Software erzeugt wird, soll kleiner als 10MB sein (Resource Utilization).
\item Der Datenverkehr, der durch die Erweiterung zusätzlich erzeugt wird, soll kleiner also 100KB/Stunde sein (Resource Utilization).
\item Die Unterbrechung der Bedienung der Software durch den Benutzer, soll nicht größer als 2 Sekunden sein (Time behaviour).
\reqend

