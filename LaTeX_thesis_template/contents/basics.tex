\section{Service Worker}\label{sec:sw}
Um die Offline Funktionalität zur Verfügung zu stellen wird ein Service Worker benutzt.
Die Service Worker API ist eine recht junge Technologie,
deren Spezifikation von der Service Worker Working Group entworfen wurde \autocite{w3c:ServiceWorker}.
Die Service Worker Working Group ist Teil des World Wide Web Consortium (W3C) und wird
geleitet von Jake Archibald und Jatinder Mann, die Mitarbeiter des Google Chrome Teams bzw. Microsoft sind.
Trotz der sich in Entwicklung befindenden API werden dennoch die
in dieser Arbeit genutzten Features von den meisten modernen Browsern unterstützt \autocite{mdn:sw-intro}.

\begin{table}[hbt]
    \centering
    \begin{adjustbox}{max width=\textwidth}
    \begin{tabular}{llllll}
        \textbf{Funktionalität} & \textbf{Chrome} & \textbf{Firefox} & \textbf{Edge} & \textbf{Safari} & \textbf{Opera} \\
        \hline \hline
        Basic support & 40 & 44 & 17 & N/A & 24 \\
        \hline
        install/activate events & 40 & 44 & 17 & N/A & Ja \\
        \hline
        fetch event/request/\lstinline|respondWith()| & 40 & 44 & 17 & N/A & N/A \\
        \hline
        caches/cache & 42 & 39 & 17 & N/A & N/A \\
        \hline
        MessageEvent & 57 & 55 & N/A & N/A & N/A \\
        \hline
        NavigationPreloadManager & 59 &  & N/A  & N/A & N/A \\
    \end{tabular}
    \end{adjustbox}
    \caption{Kompatibilität der Funktionalitäten des Service Workers in unterschiedlichen Browsern.
    Die Werte sind die Versionsnummer des Browser, ab der er diese Funktionalität unterstützt.}
    \label{table:compat}
\end{table}

Die Service Worker API wird als Nachfolger der zuvor für Offline Verfügbarkeit von Webseiten benutzten
Application Cache Api (AppCache)\footnote{https://www.w3.org/TR/2011/WD-html5-20110525/offline.html\#appcache} betrachtet.
Diese wird aufgrund zahlreicher Mängel in den Designentscheidungen dieser API
als veraltet betrachtet.
Unter diesen Mängeln befinden sich unter anderem Folgende:
\begin{itemize}
    \item Dateien werden immer vom AppCache geliefert, selbst wenn man online ist
    \item Der AppCache wird nur aktualisiert, wenn sich der Inhalt des Manifests ändert
    \item Nicht gecachte Ressourcen werden auf einer gecachten Seite nicht geladen
\end{itemize}
Es wird daher empfohlen, stattdessen auf die Service Worker API zurückzugreifen \autocite{whatwg:html}.

Standardmäßig ist JavaScript single-threaded, läuft also in einem einzelnen Thread innerhalb des Browsers.
Damit beispielsweise rechenintensiver Programmcode die Benutzeroberfläche nicht blockiert,
was zu einem schlechten Anwendererlebnis führen kann, kann dieser in einen WebWorker ausgelagert werden.
Der WebWorker läuft dann in einem eigenen Thread neben dem main thread und kann diesen somit nicht mehr blockieren.
Der Service Worker ist ein spezieller WebWorker, wird also auch in einem separaten Thread ausgeführt.
Er verhält sich wie ein Proxy zwischen dem Netzwerk und der eigentlichen Webanwendung.
Das bedeutet, nachdem die Anwendung den Service Worker registriert hat, werden alle HTTP Requests
erst vom Service Worker entgegengenommen.
Dieser kann die Requests dann bearbeiten, weiterleiten,
die Antwort behandeln und wieder zurück zur Anwendung schicken.
Dieser Vorgang geschieht transparent für den Client,
der Code im Client kann also unverändert bleiben.
Abbildung \ref{fig:sw_seq_simple} veranschaulicht diesen Ablauf.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.7\linewidth]{sw_seq_simple.pdf}
    \caption{Service Worker als Proxy bei aktiver Verbindung}
    \label{fig:sw_seq_simple}
\end{figure}

Der Service Worker arbeitet event-basiert.
Nachdem der Service Worker das Fetch Event \textsl{fetch} registriert hat, werden alle HTTP Requests abgefangen.
Falls das Durchreichen an das Netzwerk nicht funktioniert hat und dadurch auch keine Antwort vom Netzwerk zurück kommt,
kann der Service Worker entsprechend eine eigene Antwort an den Client liefern. Hierdurch entsteht das gewünschte
Verhalten in einer Offline-Umgebung. Dieses Verhalten wird in Abbildung \ref{fig:sw_seq_offline} dargestellt.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.7\linewidth]{sw_offline.pdf}
    \caption{Service Worker als Proxy bei unterbrochener Verbindung}
    \label{fig:sw_seq_offline}
\end{figure}

Dafür kann der Service Worker auf verschiedene APIs zugreifen,
unter anderem Fetch API, Cache API, Indexed Database API\@. (siehe Abschnitt \ref{subsec:arch_caching})



\subsection{Lebenszyklus}\label{subsec:lifecycle}
Um das Verhalten eines Service Workers zu kennen, muss sein Lebenszyklus verstanden werden.

Nachdem der Service Worker registriert wurde, befindet er sich in dem Zustand \textsl{waiting}.
Sobald alle Service Worker, die die Seite kontrollieren, nicht mehr aktiv sind,
geht er in den Zustand \textsl{activated} über.
Ein Service Worker kontrolliert eine Seite genau dann, wenn er von dieser registriert wurde und
sich im Zustand \textsl{activated} befindet.
So verhält es sich auch bei einem Update des Service Workers.
Bei der Registrierung eines Service Workers wird jedesmal versucht diesen erneut herunterzuladen.
Seit der Chrome Version 68 wird der Service Worker standardmäßig nicht mehr gecached,
selbst wenn er vom Backend mit einem anderen Header als \lstinline{Cache-Control: max-age=0}
ausgeliefert wird \autocite{gdev:fresher-sw}.
Falls die neu heruntergeladene und die aktuelle Version sich (bitweise) unterscheiden,
geht der neue Service Worker in den \textsl{waiting}-State solange, bis der alte
terminiert.
Der Lebenszyklus wird in Abbildung \ref{fig:sw_lifecycle} als Zustandsdiagramm dargestellt.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\linewidth]{sw_lifecycle.pdf}
    \caption{Lebenszyklus des Service Workers}
    \label{fig:sw_lifecycle}
\end{figure}

Ist der Service Worker aktiv, fängt er nur Requests von Klienten ab, die im Sichtbarkeitsbereich des
Service Workers liegen.
Der Sichtbarkeitsbereich eines Service Workers beinhaltet alle Adressen, die unter der des
Service Workers selbst liegen.
Wird der Service Worker gegen den Sichtbarkeitsbereich \lstinline{'/'} registriert, fängt er alle Requests
ab, die von der Ursprungsadresse der Anwendung aus gesendet werden.
Weiterhin kann ein Service Worker nur registriert werden, wenn er über \textsl{https} ausgeliefert wird.
Die Ursprungsadresse \lstinline{localhost} bildet hier eine Ausnahme.
Um die Entwicklung zu vereinfachen ist in diesem Fall kein \textsl{https} nötig \autocite{gdev:sw-intro}.
