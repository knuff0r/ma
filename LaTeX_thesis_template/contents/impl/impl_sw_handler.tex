Dieser Abschnitt beschreibt folgende drei Klassen: \\
\lstinline{ResponseHandler}, \lstinline{SyncHandler} und \lstinline{MessageHandler}.
Diese sind in dem \lstinline{handler} Paket enthalten.

\subsubsection{ResponseHandler}\label{subsec:impl_response_handler}
Die \lstinline{ResponseHandler}-Klasse definiert die drei Methoden
\lstinline{handleGoodAndBad(event, controller, shouldSync)},
\lstinline{handleGoodResponse(response, event, handler, data)} und
\lstinline{handleBadResponse(handler, data, shouldSync)}.

Dabei bietet \lstinline{handleGoodAndBad(event, controller, shouldSync)} die Schnittstelle nach außen und wird
bei der Definition der \textsl{Routes} in \lstinline{sw.js} als Funktion übergeben.

Die Methode \lstinline{handleGoodAndBad(event, controller, shouldSync)} ist die oberste Schicht der Requestverarbeitung.
Die Methode nimmt einen HTTPRequest entgegen und liefert eine HTTPResponse zurück.
Zunächst werden Request Informationen
wie Parameter und Body aus dem Request extrahiert (siehe Abschnitt \ref{subsubsec:impl_gapi})
und in dem Objekt \lstinline{data} zusammengefasst.
Danach wird der eigentliche HTTPRequest (verfügbar über den Parameter \lstinline{event}) versucht auszuführen.
Bei erfolgreicher Ausführung wird die resultierende HTTPResponse
zusammen mit der \lstinline{good()}-Methode des \lstinline{controller} und den extrahierten Request Daten an die Methode
\lstinline{handleGoodResponse(response, event, handler, data)} übergeben.
Bei Fehlschlag der Ausführung des HTTPRequests
wird die Methode \lstinline{handleBadResponse(handler, data, shouldSync)}, populiert mit der \lstinline{bad()}-Methode
des \lstinline{controller}, aufgerufen.

Die Methode \lstinline{handleGoodResponse(response, event, handler, data)} ruft lediglich den übergebenen
\lstinline{handler} auf und gibt die durchgereichte, vom Backend erzeugte HTTPResponse zurück.

Die Methode \lstinline{handleBadResponse(handler, data, shouldSync)} ruft ebenfalls den über-gebenen \lstinline{handler}
auf, gibt aber dessen Rückgabewert als HTTPResponse zurück.
Falls es sich bei dem Request um eine zu synchronisierende Aktion,
spezifiziert durch den Parameter \lstinline{shouldSync}, handelt, wird außerdem eine \textsl{Synchronisierungsoperation}
in der IndexedDB gespeichert.
Eine Synchronisierungsoperation hält alle Daten, die für eine spätere Synchronisation erforderlich sind.
Das sind der Body des Requests, die Endpoint ID des Requests, die Parameter und eine Referenz zu
einer Kopie der Ressource in der IndexedDB, bevor sie durch diesen Request verändert wird.
Je lokal ausgeführter, zu synchronisierender Aktion, wird also mindestens eine Synchronisierungsoperation erstellt.
Es existieren Aktionen, die mehrere solcher Operationen zur Folge haben.
Dem main thread werden dann die entstanden Synchronisierungsoperationen durch eine Message mitgeteilt.

\subsubsection{SyncHandler}\label{subsec:impl_sync_handler}
Die Klasse \lstinline{SyncHandler} bietet als äußere Schnittstelle die \lstinline{sync()}-Methode.
Sie wird durch den Erhalt einer Message vom main thread getriggert.

Der grobe Ablauf der \lstinline{sync()}-Methode wird durch das Diagramm in Abbildung \ref{fig:sync} dargestellt.
\begin{figure}[htbp]
    \centering
    \includegraphics[width=\linewidth]{sync.pdf}
    \caption{Ablauf der sync()-Methode}
    \label{fig:sync}
\end{figure}
Die Methode holt rekursiv jeweils das erste Element der Liste der gespeicherten Operationen aus der IndexedDB\@.
Sie terminiert, sobald es kein erstes Element mehr gibt, also keine Operationen mehr synchronisiert werden müssen.
Zunächst wird überprüft, ob ein Konflikt existiert.
Dies übernimmt die Methode \lstinline{checkForConflict(op, lastAnswer)}.
Sie vergleicht den Zustand der Ressource, bevor die Operation \lstinline{op} sie verändert hat, mit dem aktuellen
Zustand der Ressource im Backend.
Unterscheiden sich diese beiden Objekte, wurde ein Konflikt erkannt und
es wird eine Message an den main thread gesendet, der daraufhin einen Dialog zur
Konfliktlösung anzeigt.
Der Benutzer kann nun auswählen (siehe Abschnitt \ref{subsubsec:impl_app}), ob er den Zustand durch seine Änderungen, oder den Zustand der
Ressource auf dem Server akzeptieren will.
Die Entscheidung wird als Nachricht wieder zurück an den Service Worker geschickt.
Dieser entscheidet nun, ob die Operation direkt mittels der API, oder mit dem RTCS durchgeführt werden soll.
Im ersten Fall kann die Synchronisation weiter im Service Worker statt finden.
Handelt es sich aber um eine RTCS Operation, wird wieder eine Nachricht an den main thread gesendet,
der die empfangene Operation nun mittels des RTCS Client ausführt.
Diese Weiterleitung an den main thread ist notwendig, da WebSocket Verbindungen in Service Workern
nicht unterstützt werden.
Nach erfolgreicher Ausführung der Operation werden die neuen Informationen der Ressource aus dem Backend
verarbeitet.
Beispielsweise muss die referenzierte ID einer Ressource aller Operationen angepasst werden, die nun durch das Backend
eine neue ID zugeteilt bekommen hat.
Zuletzt wird die Operation aus dem Store in der IndexedDB entfernt und es wird mit der nächsten fortgefahren.


\subsubsection{MessageHandler}\label{subsubsec:impl_message_handler}
Die Klasse \lstinline{MessageHandler} ist dafür zuständig, eingehende Nachrichten vom main thread
zu verarbeiten, bzw. Nachrichten an diesen zu versenden.
Eine Nachricht ist ein Objekt, das aus einem Event-String \lstinline{evt}, sowie einer Argument-Liste
\lstinline{args} besteht. Die Event-Strings sind in der Klasse \lstinline{SWCommunicator}
in \lstinline{/common/SWCommunicator.js} definiert.

Das Bearbeiten der eingehenden Nachrichten übernimmt die Methode \lstinline{handleMessage(message)}.
Tabelle \ref{table:def_msg_incoming} beschreibt die Nachrichten, die vom Service Worker empfangen werden:

\begin{longtable}{p{0.1\textwidth}@{\hskip0.04\textwidth}p{0.40\textwidth}@{\hskip0.04\textwidth}p{0.42\textwidth}}
    \textbf{Event} & \textbf{Argumente} & \textbf{Bedeutung} \\
    \hline \hline
    \endfirsthead
    \textbf{Event} & \textbf{Argumente} & \textbf{Bedeutung} \\
    \hline \hline
    \endhead
    \endfoot
    \endlastfoot
    \lstinline!TOKEN! &
    \textsl{token}: Der Auth-token
    & Ein neuer QDAcity Authorisierungstoken wurde erstellt.\\
    \hline
    \lstinline!DO_SYNC! & - & Initiiere die Synchronisierung.\\
    \hline
    \lstinline!EMIT.SUCCESS! &
    \textsl{operationID}: ID der Operation die emittiert werden sollte. \newline
    \textsl{response}: Die Antwort des RTCS Servers auf den Emit.
    & Das Emittieren des RTCS war erfolgreich.
    Dies ist eine Antwort auf eine vorhergehende emit Aufforderung.\\
    \hline
    \lstinline!CONFLICT.MINE! & - & Antwort auf Aufforderung zu Lösung eines Konfliktes.
    Es soll der lokale Zustand synchronisiert werden.\\
    \hline
    \lstinline!CONFLICT.THEIRS! & - & Antwort auf Aufforderung zu Lösung eines Konfliktes.
    Es soll der auf dem Server gespeicherte Zustand beibehalten werden. Keine Synchronisation\\
    \hline
    \lstinline!CONFLICT.NONE! & - & Es gibt keinen Konflikt. Wird nur innerhalb des ServiceWorkers verwendet\\
    \hline
    \lstinline!CODING.ADD! &
    \textsl{documentId}: ID des veränderten Documents. \newline
    \textsl{operations}: Liste der angewandten slate-Operationen.
    & Es wurde das Hinzufügen eines Codes ausgelöst. Reichere die erstellte Synchronisierungsoperation zusätzlich
    mit den notwendigen Informationen an. \\
    \hline
    \lstinline!CODING.REMOVE! &
    \textsl{documentId}: ID des veränderten Documents. \newline
    \textsl{pathRange}: Slate PathRange Objekt. \newline
    \textsl{codeId}: ID des zu löschenden Codes. \newline
    & Es wurde das Entfernen eines Codes ausgelöst. Reichere die erstellte Synchronisierungsoperation zusätzlich
    mit den notwendigen Informationen an. \\
    \caption{Beschreibung der vom ServiceWorker empfangenen Nachrichten.
    Die Argumente stehen in der Reihenfolge, in der sie in der Argumentliste vorzufinden sind.} \\
    \label{table:def_msg_incoming}
\end{longtable}

Um Nachrichten an den main thread zu senden, werden die zwei Methoden \\ \lstinline{send_message_to_all_clients(msg)} bzw.
\lstinline{send_message_to_first_client(msg)} verwendet.
Dabei wird ein Nachrichtenkanal erstellt und die Nachricht auf Port 2 des Kanals an den main thread geschickt.
Bei der zweiten Methode wartet der Service Worker zusätzlich auf Port 1 auf eine Antwort.
Das bedeutet, das zurückgegebene Promise dieser Methode resolved erst, wenn eine Antwort auf Port 1 empfangen wird.
Tabelle \ref{table:def_msg_outgoing} beschreibt die Nachrichten, die vom Service Worker gesendet werden:

\begin{longtable}{p{0.16\textwidth}@{\hskip0.04\textwidth}p{0.4\textwidth}@{\hskip0.04\textwidth}p{0.36\textwidth}}
    \textbf{Event} & \textbf{Argumente} & \textbf{Bedeutung} \\
    \hline \hline
    \endfirsthead
    \textbf{Event} & \textbf{Argumente} & \textbf{Bedeutung} \\
    \hline \hline
    \endhead
    \endfoot
    \endlastfoot
    \lstinline!SYNC.STARTED! & - & Der Synchronisierungsprozess wurde gestartet.\\
    \hline
    \lstinline!SYNC.FINISHED.SUCCESS! & - & Der Synchronisierungsprozess wurde erfolgreich beendet.\\
    \hline
    \lstinline!SYNC.FINISHED.FAIL! & - & Der Synchronisierungsprozess ist fehlgeschlagen.\\
    \hline
    \lstinline!SYNC.CONFLICT! &
    \textsl{operation}: ID der Operation, die einen Konflikt ergab. \newline
    \textsl{resource-local}: Die aktuelle lokale Ressource. \newline
    \textsl{resource-backend}: Die aktuelle Ressource auf dem Server. \newline
    \textsl{diff}: Der Diff zwischen der alten, nicht veränderten lokalen Ressource und der Ressource auf dem Server.
    & Es existiert ein Konflikt. Lasse diesen lösen. \\
    \hline
    \lstinline!SYNC.RTCS! &
    \textsl{operationID}: ID der Operation, die ausgeführt werden soll. \newline
    \textsl{operation}: Operation, die ausgeführt werden soll. \newline
    & Führe diese Operation mit dem RTCS aus.\\
    \caption{Beschreibung der vom ServiceWorker gesendeten Nachrichten.
    Die Argumente stehen in der Reihenfolge, in der sie in der Argumentliste vorzufinden sind.} \\
    \label{table:def_msg_outgoing}
\end{longtable}
