Da der Service Worker auf keinen globalen Zustand zurückgreifen soll, sind alle Klassen
statisch deklariert.
Die notwendigen Daten, die über mehrere Instanzen des Service Workers hinweg,
bereitgestellt werden müssen,
d.h.\ wenn eine Transition des Service Workers aus dem Zustand \textsl{stopped} in
den Zustand \textsl{activated} existiert,
kommen dann jeweils aus der IndexedDB bzw. aus dem Cache.

\subsection{sw.js}\label{subsec:impl_sw.js}
Die Datei \textsl{sw.js} ist der zentrale Einstiegspunkt des Service Workers.
Zunächst wird das workbox Skript (siehe Abschnitt \ref{subsec:arch_wb}) importiert
und workbox-spezifische Konfigurationen vorgenommen.
Anschließend werden die Routen, die mit dem workbox.Routing Modul (siehe Abschnitt \ref{subsubsec:wb_routing}) registriert werden,
definiert.
Dabei wird unterschieden zwischen Routen, die geprecached werden können und solche, die erst zur "Laufzeit"
geroutet werden können.
Zur ersteren Kategorie gehören alle statischen Ressourcen, wie css-, js-, image- und html-Dateien.
Diese werden, bevor ein tatsächlicher Request auf diese Ressource stattfindet, gecached und erst bei einer
Veränderung ihrer Revision (die automatisch z.B.\ durch einen Hash erzeugt werden kann) versucht, neu zu laden.
Zur zweiten Kategorie gehören die API-Routen.
Eine API-Route strukturiert sich aus folgenden Eigenschaften:
\begin{itemize}
    \item \lstinline{id}
    \item \lstinline{controller|handler}
    \item \lstinline{sync}
\end{itemize}
Mittels der \lstinline{id} und dem discovery document der API, kann die URL sowie die RequestMethod berechnet werden.
Sie ist in \lstinline{common/endpoints/constants.js} definiert und entspricht der ID,
die bei der Deklaration der Endpoint-Methoden im Backend definiert wurde.
Das discovery document wird bei jedem seiner Zugriffe nach dem Prinzip network first versucht zu laden,
sofern es nicht bereits geladen ist.
Das bedeutet, sollte sich das discovery document durch Änderungen im Backend ändern, muss auch der Service Worker
aktualisiert werden, damit das discovery document neu geladen wird, da es sich ansonsten noch im Speicher befinden
könnte.
Der \lstinline{controller} ist eine eigens definierte Controller Klasse(siehe Abschnitt \ref{subsec:impl_controller}),
die dieser Route zugeordnet werden soll.
Alternativ kann auch eine vordefinierte Strategie aus workbox.Strategies
(siehe Abschnitt \ref{subsubsec:wb_strats}) als \lstinline{handler} zugewiesen werden.
Ist das \lstinline{sync}-flag gesetzt, wird beim Ansprechen dieser Route eine entsprechende Operation zur
Synchronisierung gespeichert (siehe Abschnitt \ref{subsec:impl_response_handler})

Weiterhin wird in diesem Script auf das \lstinline{message}-Event gelauscht, dessen \lstinline{data}-Attribut an den
MessageHandler weitergereicht wird (siehe Abschnitt \ref{subsubsec:impl_message_handler})




\subsection{Controller}\label{subsec:impl_controller}
Die Controller Klassen folgen alle dem gleichen Schema. Jeder Controller, der einem Endpoint zugeordnet wird,
implementiert zwingend eine \lstinline{good(resource, data)} und eine \lstinline{bad(data)} Methode.

Die \lstinline{good(resource, data)} Methode wird aufgerufen, wenn der entsprechende Request erfolgreich ausgeführt wurde,
d.h.\ eine aktive Internetverbindung besteht. Deshalb kann dieser Methode auch das \lstinline{resource}-Objekt
übergeben werden, das den Body der HTTPResponse enthält. Das \lstinline{data}-Objekt enthält alle Parameter
des HTTPRequests als Schlüssel-Wert Paare, sowie den Body des HTTPRequests und die ID des Endpoints.
Die meisten dieser \lstinline{good()}-Methoden cachen die Ressource in der IndexedDB (bzw. entfernen diese), passen äquivalent
zum Backend-Code ggf. abhängige Ressourcen an und geben die vom Backend übergebene Ressource in einem Promise zurück.

Die \lstinline{bad(data)}-Methode wird aufgerufen, wenn der entsprechende Request fehlgeschlagen ist, also keine aktive
Internetverbindung besteht.
Folglich kann dieser Methode auch nicht die Ressource vom Backend übergeben werden.
Diese Methode übernimmt dann zusätzlich auch die komplette Erstellung der Ressource (inklusive der Generierung von IDs) und gibt
diese ebenfalls in einem Promise zurück.
Da es sich hier nun um eine lokal erstellte Ressource handelt, wird dieser absteigend eine negative ID
zugewiesen und zur Kennzeichnung mit einem \textsl{dirty}-flag versehen. Hierbei ist noch zu beachten, dass die
Struktur der zurückgegeben Ressource konform mit jener vom GAPI Endpoint ist.
Beispielsweise wird ein Array noch in ein Objekt, das dieses als \lstinline{items} definiert, gehüllt.

Auf diese Weise kann für jeden Endpoint, der behandelt werden soll, eine zugehörige \lstinline{Controller}-Klasse
erstellt werden.

\subsection{DB}\label{subsec:impl_db}
Wie in Abschnitt \ref{subsec:arch_caching} beschrieben, wird zur Speicherung der Ressourcen die IndexedDB
verwendet. Zur Abstrahierung der Zugriffe dient die Klasse \lstinline{DB}. Sie kapselt alle nötigen Operationen
auf der IndexedDB und stellt dafür eigene Methoden bereit.
Wenn nicht anderes spezifiziert, erfolgen alle Zugriffe auf die Datenbank, die durch die ID des sich aktuell
im Cache befindlichen Benutzers angesprochen wird.
Um die einzelnen Stores zu erzeugen, wird bei jedem Öffnen der Datenbank überprüft, ob sich die Datenbankversion
(spezifiziert in \lstinline{db/constants.js}) erhöht hat.
Falls dies der Fall ist, wird durch alle Stores (ebenfalls definiert in \lstinline{db/constants.js})
iteriert und eventuell noch nicht vorhandene Stores werden erzeugt.
Um einen weiteren Store hinzuzufügen, muss also lediglich der Store definiert und die \lstinline{DB_VERSION}
inkrementiert werden.
Ein Store folgt dabei folgendem Schema:
\begin{itemize}
    \item \lstinline{name}: Name des Stores
    \item \lstinline{options}: Options-Objekt aus der IndexedDB-Spezifikation
    \item \lstinline{createCopy -> true|false}: Optional.
    Falls \lstinline{true}, wird neben diesem Store ein weiterer Store erzeugt, der Kopien der Ressourcen enthält, bevor diese durch
    eine Aktion verändert werden.
\end{itemize}

Um das tatsächlich verwendete Persistenzverfahren zu entkoppeln, wird der Zugriff durch die \lstinline{Controller}-Klassen
noch einmal über die \lstinline{Service}-Klassen Schicht gekapselt.
Jede \lstinline{Service}-Klasse erbt dabei von der \lstinline{CrudService}-Klasse.
Diese definiert wiederum alle nötigen Zugriffsmethoden unter Benutzung des Attributes \lstinline{STORE_NAME}.
Für jeden Store existiert eine \lstinline{Service}-Klasse und im allgemeinen und einfachsten Fall unterscheiden sich
diese lediglich durch den \lstinline{STORE_NAME}.
Abbildung \ref{fig:db_class} stellt diesen Zusammenhang in einem Klassendiagramm dar.
Das Diagramm beinhaltet nicht alle Kindklassen von \lstinline{CrudService}.
\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{db_class.pdf}
    \caption{Klassendiagramm der Datenbank-Komponente}
    \label{fig:db_class}
\end{figure}



\subsection{Handler}\label{subsec:impl_handler}
\input contents/impl/impl_sw_handler.tex


\subsection{Util}\label{subsec:impl_util}

\subsubsection{Gapi}\label{subsubsec:impl_gapi}
Die \lstinline{Gapi}-Klasse stellt Methoden zur Verfügung, um Requests anhand der Endpoint-ID auszuführen.
Gegenüber der GoogleApi Client Library für Node.js \footnote{https://github.com/googleapis/google-api-nodejs-client}
bietet diese Klasse noch zwei weitere Funktionalitäten.
Zum einen kann mit der Methode \lstinline{getRegex(id)} ein API-Endpoint anhand seiner \lstinline{id} in
einen regulären Ausdruck transformiert werden.
Dies ist notwendig, da die \lstinline{registerRoute()}-Methode des workbox.routing Moduls unter anderem einen
regulären Ausdruck erwartet.
\begin{lstlisting}[language=Javascript]
new RegExp(path.replace(/{\w+}/g, "\\w+") + "(\\?.*)?$");
\end{lstlisting}
Beispielsweise wird also eine \lstinline{id},
die in einen vom discovery document definierten \lstinline{path}\textrightarrow
\textsl{"relocateCode/\{codeId\}/\{newParentID\}"} resultiert, in folgenden regulären Ausdruck umgewandelt:
\lstinline{/relocateCode\/\w+\/\w+(\?.*)?$/}.
Dieser sorgt dann dafür, dass eine tatsächliche URL auf die entsprechende API-Route gematcht wird.

Die zweite Methode \lstinline{getRequestData(request)} extrahiert aus einem HTTPRequest, mithilfe des
geladenen discovery documents, ein Objekt der Form \lstinline|{id, params, body}|,
wobei das \lstinline{params}-Objekt die Attribute mit den Werten aus dem HTTPRequest beinhaltet,
die strukturell im discovery document definiert sind.

