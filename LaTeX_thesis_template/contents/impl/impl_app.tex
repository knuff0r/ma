Die im Folgenden beschriebenen Anpassungen wurden alle im Quellcode der Frontend Anwendung,
die im main thread läuft, vorgenommen.

Zuerst muss der Service Worker registriert werden.
Dies geschieht in der Funktion \lstinline{initServiceWorker()} in \lstinline{index.js}.
Falls die Registrierung zwar erfolgreich war, der \lstinline{serviceWorker.controller}
aber nicht existiert, wird die Seite neu geladen. Das kann der Fall sein, wenn die Seite
durch einen Force Refresh geladen wurde.


\subsection{SyncService}\label{subsec:implApiservice}
Nicht alle Aktionen werden über die Google Client Library mittels HTTPRequest realisiert.
Mit der Erweiterung der Anwendung durch den RTCS werden bestimmte Aktionen über Socket Messages
von dem RTCS Client an den RTCS Server ausgeführt.
Da der Service Worker diese Messages, im Gegensatz zu HTTPRequests, nicht abfängt, muss der bisherige
Mechanismus angepasst werden.

Zuerst wird der Fall bei aktiver Verbindung behandelt.
Der RTCS Client schickt über den WebSocket die Nachricht an den RTCS Server.
Dieser schickt eine Antwort mit der resultierenden Ressource.
Der Client registriert diese Antwort in einem Event und benachrichtigt alle nötigen
Komponenten der Anwendung über die erfolgreiche Durchführung dieser Aktion und der zurückgegeben
Ressource.
Das geschieht in der Methode \lstinline{fireEvent(evt, ...args)} in \lstinline{index.js}
im RTCS Client.
Hier wird dann zusätzlich eine Nachricht an den Service Worker mit dem Event und den Argumenten
geschickt, damit dieser dann die zurückgegeben Ressource beispielsweise cachen kann.

Ohne aktive Internetverbindung kann die Message vom RTCS Client über den WebSocket an der
RTCS Server erst gar nicht gesendet werden.
Der \lstinline{emit(eventName, args, ack)}-Methode von Socket.js wird eine Callback Funktion
\lstinline{ack} übergeben.
In dieser Callback Funktion wird nun ein Flag gesetzt, dass die Nachricht erfolgreich an den RTCS Server gesandt wurde.
Falls also die Message nicht zugestellt werden kann, da keine Verbindung existiert, wird dieses Callback nie
ausgeführt.
Die \lstinline{emit()}-Methode wird also in jedem Fall aufgerufen.
Direkt nach der Ausführung wird jedoch mittels \lstinline{Socket.disconnected}
geprüft, ob der Socket eine Verbindung hat.
Ist das nicht der Fall, tritt die Fallback Behandlung ein.
Es kann aber vorkommen, dass dieser direkt nach der Ausführung der \lstinline{emit()}-Methode Socket
noch den Zustand \lstinline{Socket.connected=true} aufweist, obwohl die Nachricht nicht gesendet werden konnte.
Deshalb wird noch drei Sekunden gewartet, ob die Callback Funktion aufgerufen und somit das entsprechende
Flag auf \lstinline{true} gesetzt wurde.
Ist es \lstinline{false}, tritt ebenfalls die Fallback Behandlung ein.
Die Fallback Behandlung besteht aus der Klasse \lstinline{ApiService} im RTCS Client.
Diese implementiert, äquivalent zu der existierenden \lstinline{emit()}-Methode,
eine \lstinline{emit(messageType, arg)}-Methode, welche dem \lstinline{messageType} einen entsprechenden
API Methodenaufruf über die Google Client Library zuordnet.
Dieser wird dann wie gehabt vom Service Worker abgefangen.
SocketIO speichert die Nachrichten, die mit \lstinline{emit()} versucht wurden zu versenden,
in einem Buffer.
Da die Nachricht bereits durch den Fallback behandelt wurde,
soll bei Wiederherstellung der Verbindung nicht nochmals versucht werden diese Nachricht zu verschicken.
Deshalb wird bei Eintreten der Fallback-Behandlung dieser Buffer geleert.

Einerseits wird so das Problem bei unterbrochener Netzwerkverbindung gelöst. Andererseits kann damit eine
durch den RTCS behandelte Aktion schneller ausgeführt werden,
falls dieser für seine Antwort länger als drei Sekunden brauchen sollte.
Dadurch wird die Benutzerfreundlichkeit der Anwendung erhöht.

\subsection{App Component}\label{subsubsec:impl_app}
In der React Komponente \lstinline{App.jsx} werden alle weiteren nötigen
Operationen ausgeführt bzw. an Unterkomponenten verteilt.
Diese Änderungen betreffen unter anderem die Kommunikation zum Service Worker, die UI,
sowie die Kommunikation zum RTCS Client.


\subsubsection{UI}\label{subsubsec:gui}

Damit dem Benutzer ersichtlich ist, dass er momentan keine Verbindung zum Server besitzt,
wird ihm dies in der Navigationsleiste angezeigt (siehe Abbildung \ref{fig:navbar})

\begin{figure}[H]
    \includegraphics[width=1.0\linewidth]{navbar.png}
    \caption{Navigationsleiste bei unterbrochener Verbindung.}
    \label{fig:navbar}
\end{figure}

Im HTML5 Standard existiert zwar das Event \lstinline{'offline'},
ist aber je nach Browser unterschiedlich implementiert und bezieht sich meist auf den browser-eigenen `Offline`-Modus.
Um wirklich festzustellen, ob eine Verbindung zum Server existiert, wird dieser in einem Intervall von
drei Sekunden angepingt.
Dies erzeugt eine Request/Response mit einer Größe von ca. 50 Bytes.
Schlägt dieser Request nun fehl, existiert keine Verbindung zum Backend.

Eine weitere graphische Komponente, die dem Nutzer bei der Verwendung der Anwendung ohne Verbindung hilfreich sein kann,
ist die Operationsliste.
Wenn eine unterstützte Aktion lokal ausgeführt wird,
wird die entsprechende Operation zur Synchronisation dem Benutzer in einer Liste dargestellt
(siehe Abbildung \ref{fig:operations}).
Somit weiß der Benutzer, welche Änderungen er vorgenommen hat,
die bei Verbindungswiederherstellung synchronisiert werden.
\begin{figure}[H]
    \includegraphics[width=.7\linewidth]{operations.png}
    \caption{List der lokalen, nicht synchronisierten Operationen.}
    \label{fig:operations}
\end{figure}


Falls bei der Synchronisation im Service Worker ein Konflikt auftritt, schickt dieser dem main thread eine
Nachricht mit den jeweiligen Ressourcen und dem Diff, damit der Konflikt für den Benutzer dargestellt werden kann.
Dieser kann mithilfe eines Dialogs entscheiden, wie der Konflikt gelöst werden soll.

\begin{figure}[H]
    \includegraphics[width=1.0\linewidth]{conflict.png}
    \caption{Dialog zur Lösung des Konflikts bei unterbrochener Verbindung.}
    \label{fig:conflict}
\end{figure}

Der Benutzer kann hier zwischen zwei Optionen wählen.
Entweder kann er seine Änderungen, die er lokal bereits durchgeführt hat, auch auf dem Server
übernehmen (links).
Oder er kann die Version auf dem Server akzeptieren und sein lokalen Änderungen dadurch überschreiben (rechts).
Der Unterschied der Versionen der Ressource auf dem Server und der Version vor der lokalen Änderung
wird durch eine Markierung des entsprechenden Attributs dargestellt.

\subsubsection{Kommunikation Service Worker und RTCS Client}\label{subsubsection:comm}
Weiterhin wurde der bestehende Authentifikationsmechanismus erweitert, sodass bei Erneuerung des
Authentifikationstokens dieser an den Service Worker gesendet werden kann.

Soll die zu synchronisierende Operation mittels des RTCS ausgeführt werden,
schickt der Service Worker eine Nachricht mit der Operation an den main thread.
Dieser übersetzt dann die Operation wieder in einen Aufruf an den RTCS Client.
