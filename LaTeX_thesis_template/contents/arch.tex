\section{Allgemein}\label{sec:arch_general}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\linewidth]{arch.pdf}
    \caption{Kommunikation der beteiligten Komponenten in QDAcity}
    \label{fig:arch}
\end{figure}

Abbildung \ref{fig:arch} zeigt die an QDAcity beteiligten Komponenten und stellt
die Kommunikation zwischen diesen dar.
Vor der Implementierung des Service Workers kommunizierte die QDAcity Frontend Anwendung
mit dem QDAcity Backend über die sich im main thread befindliche Google Client Library(GAPI)
bzw. über den RTCS (Realtime Collaboration Service) Client.
Da der Service Worker gegen die Ursprungsadresse registriert wird, werden alle
Requests, die von der GAPI Library gesendet werden, vom Service Worker abgefangen.
Requests, die durch nicht unterstützte Aktionen entstanden sind, werden trotzdem
durch den Service Worker geleitet.
Dieser ignoriert diese Requests aber, leitet sie also unmodifiziert und unbehandelt and das Netzwerk weiter
und gibt die Antwort des Netzwerks unbearbeitet zurück.
Für Aktionen, die mittels des RTCS ausgeführt werden, geschieht die Kommunikation nicht direkt zwischen dem
RTCS Client und dem Service Worker, sondern ebenfalls über die durch den GAPI Library entstandenen HTTPRequests.
Zum Nachrichtenaustausch, der nicht über HTTPRequests realisiert wird,
kann der Service Worker außerdem direkt mit dem main thread kommunizieren.

\subsection{Caching und Persistenz}\label{subsec:arch_caching}

Um die geforderten Ressourcen offline bereitstellen zu können, müssen diese lokal persistiert werden.
Der HTML5 Standard spezifiziert dafür verschiedene Möglichkeiten:
\begin{itemize}
    \item \textbf{WebSQL}
    bietet relationale Datenbanken, ist jedoch veraltet bzw.
    wird nicht mehr aktiv entwickelt \autocite{w3c:websql}.
    \item \textbf{Web Storage}
    bietet mit \textsl{localStorage} und \textsl{SessionStorage} einfache Schlüssel-Wert Speicher,
    der jedoch, da er nicht indiziert ist, nur ineffizient durchsucht werden kann. Er arbeitet außerdem synchron und
    ist weder in Web Workern noch in Service Workern verfügbar.
    \item \textbf{IndexedDB API}
    bietet einen indizierten Schlüssel-Wert Speicher. Dieser lässt sich dank Indizierung
    effizient über die Einträge durchsuchen.
    \item \textbf{Cache Storage},
    definiert in der Service Worker Spezifikation,
    bietet eine Schnittstelle zur Speicherung von Request/Response-Paaren. Dieser Cache ist unabhängig vom
    Browser-Cache \autocite{w3c:sw:cache}.
    \item \textbf{Application Cache}
    bietet, ähnlich wie der Cache Storage, die Möglichkeit, Ressourcen zu cachen.
    Dieses Interface ist jedoch veraltet (siehe Abschnitt \ref{sec:sw}).
\end{itemize}

Daraus ergibt sich, dass für dieses Projekt zwei verschieden Speicher genutzt werden.
Zum einen wird der \textbf{Cache Storage} verwendet, um statische Ressourcen zu cachen, unter anderem verschiedene
JavaScript, CSS oder HTML Dateien.
Zum anderen wird daneben die \textbf{IndexedDB API} benutzt, um dynamische Ressourcen zu speichern.
Sie stellt das lokale Gegenstück zur Datenbank dar, auf die das QDAcity Backend zurückgreift.
Die IndexedDB ist in mehrere \textsl{Databases} unterteilt,
die wiederum einer Ursprungsadresse (z.B qdacity.com oder localhost) zugehörig sind.
Die Möglichkeit, mehrere \textsl{Databases} zu benutzen, wird hier nun dafür genutzt,
die Daten verschiedener Nutzer zu speichern.
Das bedeutet, für jeden authentifizierten Benutzer von QDAcity wird eine Database in der IndexedDB angelegt.
Diese wird dann anhand der ID des Benutzers diesem zugeordnet.
Je Ressourcetyp wird dann ein \textsl{Store} in einer dieser Databases angelegt.

Abbildung \ref{fig:idb} veranschaulicht diesen Aufbau am Beispiel der Domäne qdacity.com und zwei Benutzern
mit jeweils zwei Stores.
\begin{figure}[htbp]
    \centering
    \includegraphics[width=\linewidth]{idb.pdf}
    \caption{Aufbau der IndexedDB}
    \label{fig:idb}
\end{figure}


\subsubsection{Quota}\label{subsubsec:quota}
Bei der Verwendung der jeweiligen Storage APIs muss jedoch auch der in Anspruch genommene Speicherplatz
berücksichtigt werden. Die Regeln, wie viel Speicher welcher Storage einnehmen darf, ist allerdings browser-abhängig.

Bei Firefox werden zum Zweck der Speicherplatzberechnung die unterschiedlichen Storage APIs
zu einem \textbf{browser storage} zusammengefasst.
Darunter fallen dann auch unter anderem die Cache API und
die IndexedDB API. Es wird ferner zwischen \textbf{global limit} und \textbf{group limit} unterschieden.
Das global limit berechnet sich aus 50\% des freien Festplattenspeicherplatzes.
Das group limit bezeichnet den Speicherplatz, den eine bestimmte Domain zur Verfügung hat.
Im Fall von QDAcity wäre das qdacity.com. Dieses berechnet sich aus 20\% des global limits, jedoch maximal 2 GB
\autocite{mdn:idb:quota}.

Um die Arbeit mit der IndexedDB API zu erleichtern, wurde das
Packet \textsl{idb}\footnote{https://github.com/jakearchibald/idb} von Jake Archibald,
Mitentwickler der Service Worker Spezifikation, verwendet.
Dieses wandelt \textsl{IDBRequests} in Promises um.


\subsection{Workbox}\label{subsec:arch_wb}
Wie in der Einführung zum Service Worker beschrieben, besteht dieser aus einem einzelnen Javascript File.
Der Service Worker lauscht dabei vor allem auf das \textsl{fetch-Event}.
Für die grundlegenden Funktionalitäten existiert die Javascript Library
Workbox\footnote{https://developers.google.com/web/tools/workbox/}.
Sie ist der Nachfolger der Libraries sw-toolbox\footnote{https://googlechromelabs.github.io/sw-toolbox}
und sw-precache\footnote{https://github.com/GoogleChromeLabs/sw-precache}.
Sie vereint und erweitert deren Funktionalitäten basierend auf einer moderneren Codebasis.
Workbox wird aktiv, unter anderem von Mitarbeitern aus dem Google Web Developer Relations Team, entwickelt
\autocite{chromelabs:sw-toolbox}.


Die Workbox Library ist ebenfalls ein einzelnes Javascript File, welches in das Service Worker File importiert
werden kann.
Sie gliedert sich neben dem Modul \lstinline{workbox.core} in weitere Module,
von denen von Folgenden Gebrauch gemacht wird:

\subsubsection{Workbox.Routing}\label{subsubsec:wb_routing}
Anstatt im \textsl{fetch}-event Listener mit einer großen Verzweigung über die kommende URL den Quellcode zu strukturieren,
kann mittels des workbox.routing Moduls einfach die entsprechende URL registriert werden.
Der bei der Behandlung der URL auszuführende Code wird durch eine Funktion dargestellt.
Damit URL Schablonen der tatsächlichen URL zugeordnet werden können,
können die zu unterstützenden URLs als regulärer Ausdruck übergeben werden \autocite{wb:routing}.
Durch die Verwendung dieses Moduls kann der Quellcode wesentlich übersichtlicher strukturiert werden.

\subsubsection{Workbox.Strategies}\label{subsubsec:wb_strats}
Das Modul \lstinline{workbox.strategies} bietet vordefinierte Handler für übliche Caching-Strategien.
Diese werden benutzt, falls keine komplexere Logik, wie sei bei den API-Routen
(siehe Abschnitt \ref{subsec:impl_sw.js} und \ref{subsec:impl_controller}) vorliegt, notwendig ist.
Dabei stehen folgende Strategien zur Verfügung:
\begin{itemize}
    \item Stale-While-Revalidate
    \item Cache First
    \item Network First
    \item Network Only
    \item Cache Only
\end{itemize}
Stale-While-Revalidate versucht zuerst mit der Ressource aus dem Cache zu antworten.
Wenn sie nicht verfügbar ist, wird auf das Netzwerk zurückgegriffen.
In jedem Fall entsteht ein Netzwerkanfrage, damit die Ressource dann gecached werden kann.

Cache First unterscheidet sich zu Stale-While-Revalidate insofern, dass die Ressource nur dann gecached wird,
wenn sie noch nicht gecached war. Das bedeutet, falls die Ressource aus dem Cache geliefert wird, wird kein
zusätzlicher Request an das Netzwerk gesendet und die Ressource wird im Cache nicht aktualisiert.

Network First versucht zuerst die Ressource aus dem Netzwerk zu holen und cached diese dann.
Falls das nicht gelingt, wird auf den Cache zurückgegriffen.
\autocite{wb:strategies}

Die letzten beiden Strategien sind eher ungebräuchlich und werden deshalb auch nicht weiter erläutert.

Während der Entwicklung wurden alle Requests von statischen Routen (siehe Abschnitt \ref{subsec:impl_sw.js})
mit der Strategie \textsl{Network First} bedient.



\subsubsection{Workbox.Precaching}\label{subsubsec:wb_precache}
Das Modul \lstinline{workbox.precaching} erlaubt es, Ressourcen mit dem Service Worker zu cachen,
bevor diese tatsächlich angefragt werden.
Der Service Worker lädt die benötigten Ressourcen herunter und speichert diese im Cache.
Weiterhin wird eine Revisionsinformation für jede Ressource in der IndexedDB gespeichert.
Dieser Vorgang findet im \textsl{install}-Event des Service Workers statt.
Basierend auf den Revisionsinformationen werden dann nur die Ressourcen aktualisiert,
deren Revision sich geändert hat.
Zur automatisierten Anpassung der Revision sollte diese aus dem Hashwert der entsprechenden Dateien
bestehen.
Es existieren Tools, die diese Funktion bereits implementiert haben,
unter anderem das Plugin \textsl{workbox-webpack-plugin}\footnote{https://www.npmjs.com/package/workbox-webpack-plugin}
\autocite{wb:precaching}





